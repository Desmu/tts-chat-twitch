# TTS Chat Twitch

**Ce projet ne sera plus mis à jour. L'outil [Twitchat](https://twitchat.fr/) inclut néanmoins cette fonctionnalité.**
Script pour lire les messages du chat Twitch d'un ou de plusieurs comptes en Text To Speech.

## Installation

1. Télécharger [Greasemonkey pour Firefox](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/) ou [Tampermonkey pour Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo), des plugins pour le navigateur permettant d'exécuter ses propres scripts sur une page web.
2. Ouvrir ensuite le script en cliquant sur ce lien : [`tts-chat-twitch.user.js`](https://gitlab.com/Desmu/tts-chat-twitch/-/raw/master/tts-chat-twitch.user.js), le plugin devrait automatiquement proposer son installation.
3. L'interface permettant d'ajouter des boutons devrait apparaître sous le chat de tous les streams Twitch et dans leur version popup.

## Utilisation

Le plugin ajoute un champ de texte dans l'interface du chat, entrez-y le ou les pseudos des comptes (séparés par des virgules) à faire parler. Les changements sont appliqués en temps réel.

## Récupérer les voix installées sous Windows

Sous Windows, certaines voix ne seront potentiellement pas détectées. Pour y rémédier, ouvrir Powershell en tant qu'administrateur, y copier ceci, puis redémarrer le PC :
```powershell
$sourcePath = 'HKLM:\software\Microsoft\Speech_OneCore\Voices\Tokens' #Where the OneCore voices live
$destinationPath = 'HKLM:\SOFTWARE\Microsoft\Speech\Voices\Tokens' #For 64-bit apps
$destinationPath2 = 'HKLM:\SOFTWARE\WOW6432Node\Microsoft\SPEECH\Voices\Tokens' #For 32-bit apps
cd $destinationPath
$listVoices = Get-ChildItem $sourcePath
foreach($voice in $listVoices)
{
$source = $voice.PSPath #Get the path of this voices key
copy -Path $source -Destination $destinationPath -Recurse
copy -Path $source -Destination $destinationPath2 -Recurse
}
```
(Merci [StackOverflow](https://stackoverflow.com/a/47485786))
