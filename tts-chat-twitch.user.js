// ==UserScript==
// @name        TTS Chat Twitch
// @namespace   https://www.twitch.tv/
// @description Script pour lire les messages du chat Twitch d'un ou de plusieurs comptes en Text To Speech.
// @match       https://www.twitch.tv/*
// @match       https://www.twitch.tv/popout/*/chat*
// @version     1.0.1
// @grant       none
// @author      Desmu
// @homepageURL https://gitlab.com/Desmu/tts-chat-twitch
// @updateURL   https://gitlab.com/Desmu/tts-chat-twitch/-/raw/master/tts-chat-twitch.user.js
// @downloadURL https://gitlab.com/Desmu/tts-chat-twitch/-/raw/master/tts-chat-twitch.user.js
// ==/UserScript==

let voices = [];

function refreshVoices () {
    voices = window.speechSynthesis.getVoices();
    voices.sort((voiceA, voiceB) => {
        if (voiceA.name > voiceB.name) {
            return 1;
        }
        if (voiceB.name > voiceA.name) {
            return -1;
        }
        return 0;
    });
    let voicesHTML = "";
    voices.forEach((voice) => {
        let isSelected = "";
        if (voice.lang === "fr-FR") {
            isSelected = "selected";
        }
        voicesHTML += `<option value="${voice.voiceURI}" ${isSelected}>${voice.name}</option>`;
    });
    document.querySelector("#voices-tts").innerHTML = voicesHTML;
}

function callback (mutationList, observer) {
    mutationList.forEach((mutation) => {
        switch (mutation.type) {
        case "childList":
            if (mutation.addedNodes && (mutation.addedNodes.length > 0) && (mutation.addedNodes[0].querySelectorAll(".chat-line__username-container").length > 0) && (mutation.addedNodes[0].querySelectorAll(".text-fragment").length > 0) && (mutation.addedNodes[0].className === "chat-line__message")) {
                let pseudos = document.querySelector("#pseudo-tts").value.toLowerCase().split(",");
                pseudos = pseudos.map((pseudo) => pseudo.trim());
                if (pseudos.indexOf(mutation.addedNodes[0].querySelector(".chat-line__username-container").innerText.toLowerCase()) !== -1) {
                    const tts = new SpeechSynthesisUtterance(mutation.addedNodes[0].querySelector(".text-fragment").innerText);
                    const foundVoice = voices.find(({name}) => name === document.querySelector("#voices-tts").options[document.querySelector("#voices-tts").selectedIndex].text);
                    if (foundVoice) {
                        tts.voice = foundVoice;
                    } else {
                        tts.lang = "fr-FR";
                    }
                    window.speechSynthesis.speak(tts);
                }
            }
            break;
        case "attributes":
            break;
        default:
            break;
        }
    });
}

window.addEventListener("load", () => {
    if (document.querySelector(".stream-chat").length !== 0) {
        const div = document.createElement("div");
        div.className = "tw-flex tw-flex-row";
        div.innerHTML = "<input id='pseudo-tts' type='text' style='width: 200px'><select id='voices-tts' style='width: 200px'></select>";
        document.querySelector(".chat-input").append(div);
        if ("speechSynthesis" in window) {
            refreshVoices();
            window.speechSynthesis.onvoiceschanged = () => {
                refreshVoices();
            };
        }
        (new MutationObserver(callback)).observe(document.querySelector(".chat-list--default").children[0], {"childList": true, "subtree": true});
    }
});
